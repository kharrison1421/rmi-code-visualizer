package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

import visualizer.flowerVisualizer.FlowerVisualizer;
import visualizer.flowerVisualizer.FlowerVisualizerImpl;
import visualizer.parser.CodeDataObject;
import visualizer.parser.Parser;
import visualizer.translator.ClassTranslatorImpl;
import visualizer.translator.TranslatedObjects;
import compute.Compute;

public class VisualizeCode extends JPanel implements ActionListener{

	private static final long serialVersionUID = 4518728334363342421L;
	private static final String HOST = "localhost";
	private static final int PORT = 64658;
	
	private static Registry registry; // Registry on server keeping track of created objects
	private static Compute comp; // Compute engine that will execute given tasks
	private static String path; // Path to source code input through file chooser dialog box
	
	// Components for file chooser dialog box
	private JButton go;   
	private JFileChooser chooser;
	private String choosertitle;
	
	/**
	 * Sets up file chooser button and action listener
	 */
	public VisualizeCode() {
		go = new JButton("Select Directory");
		go.addActionListener(this);
		add(go);
	}
	
	/**
	 * Action listener for file chooser button. Opens file chooser, gets input directory, 
	 * then calls the parser, translator, and visualizer.
	 */
	public void actionPerformed(ActionEvent e) {
		// Sets up file chooser dialog
		chooser = new JFileChooser(); 
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle(choosertitle);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		// disable the "All files" option.
		chooser.setAcceptAllFileFilterUsed(false); 

		// If user selects an open, use input path for visualizer. Else, do nothing.
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
			path = chooser.getCurrentDirectory().toString();
			System.out.println("actionPerformed: Path selected: " + path);
			
			try {
				// Create parser object using given path and send it to compute engine on server to execute
				Parser parseTask = new Parser(path);
				CodeDataObject data = comp.executeTask(parseTask);
				System.out.println("Parser executed");
				
				// Create translator object using data object returned by parser and send it to compute 
				// engine on server to execute.
				ClassTranslatorImpl translateTask = new ClassTranslatorImpl(data);
				TranslatedObjects translated = comp.executeTask(translateTask);
				System.out.println("Translator executed");
				
				// Create visualizer object using translated object returned from translator and use it to
				// draw flowers
				FlowerVisualizer visualizer = new FlowerVisualizerImpl(translated);
				visualizer.drawFlowers();
				System.out.println("Visualizer executed");
			} catch (RemoteException e1) {
				System.out.println("Error in VisualizeCode.actionPerformed try block");
				e1.printStackTrace();
			}
		}
		else {
			System.out.println("actionPerformed: No selection");
		}
	}
	
	/**
	 * Sets up dialog window containing file chooser
	 */
	public static void getPathInput(){
		VisualizeCode panel = new VisualizeCode();
		panel.setSize(100, 100);
		
		JFrame frame = new JFrame("Code Visualizer");
		frame.setSize(200, 200);
		frame.add(panel);
		frame.setVisible(true);
		frame.addWindowListener(
			new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			}
		);
	}

	public static void main(String args[]) {
        try {
        	// Get access to compute engine on server
            String name = "Compute";
            registry = LocateRegistry.getRegistry(HOST, PORT);
            comp = (Compute) registry.lookup(name);
            
            getPathInput();
        } catch (Exception e) {
            System.err.println("Exception in main:");
            e.printStackTrace();
        }
    }    
}
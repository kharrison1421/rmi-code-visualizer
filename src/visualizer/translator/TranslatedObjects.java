package visualizer.translator;

import java.io.Serializable;
import java.util.ArrayList;

import visualizer.flowerVisualizer.FlowerComposite;
import visualizer.flowerVisualizer.FlowerRelation;

/**
 * Object encapsulating flower objects and their relationships created by Translator
 * 
 * @author Kieran Harrison
 *
 */
public class TranslatedObjects implements Serializable{
	
	private static final long serialVersionUID = -847733248112827461L;
	
	private FlowerComposite composite; // Object encapsulating packages and flowers created by Translator
	private ArrayList<FlowerRelation> relations; // List of relationships between flowers
	private int topStemHeight; // Height of tallest flower
	
	public TranslatedObjects(
			FlowerComposite composite, ArrayList<FlowerRelation> relations, int topStemHeight
	){
		this.composite = composite;
		this.relations = relations;
	}
	
	public FlowerComposite getComposite(){
		return composite;
	}
	
	public ArrayList<FlowerRelation> getRelations(){
		return relations;
	}
	
	public int getTopStemHeight(){
		return topStemHeight;
	}

}

package visualizer.translator;
import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import compute.Task;
import visualizer.flowerVisualizer.Flower;
import visualizer.flowerVisualizer.FlowerComposite;
import visualizer.flowerVisualizer.FlowerRelation;
import visualizer.parser.ClassObject;
import visualizer.parser.CodeDataObject;

public class ClassTranslatorImpl implements ClassTranslator, Task<TranslatedObjects>, Serializable{
	
	private static final long serialVersionUID = -2883739916036545946L;
	
	private final float MAXSTEMHEIGHT = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height / 3;
	private final int MINSTEMHEIGHT = 0;
	
	private static FlowerComposite packages;
	private static ArrayList<FlowerRelation> relationList;
	
	private CodeDataObject dataObject;
	private int topStemHeight = 0;
	
	public ClassTranslatorImpl(CodeDataObject dataObject){
		this.dataObject = dataObject;
	}
	
	@Override
	public void translateClass(ArrayList<ClassObject> classes) {
		for(int i = 0; i < classes.size(); i++){
			if(classes.get(i).getNumberOfLines() > topStemHeight){
				topStemHeight = classes.get(i).getNumberOfLines();
			}
		}

		float stemHeightFactor = MAXSTEMHEIGHT / topStemHeight;
		
		Random rand = new Random();

		packages = new FlowerComposite();
//		packages.setName(classes.get(0).getPackage().split("\\.")[0]);
		packages.setName(classes.get(0).getPackage().substring(0,classes.get(0).getPackage().lastIndexOf(".") + 1));
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		packages.setPrimaryColor(new Color(r, g, b, 0.5f));
		
		FlowerComposite newPackage = new FlowerComposite();
		newPackage.setName("");
		
		for(int i = 0; i < classes.size(); i++){
//			String newPackageName = classes.get(i).getPackage().split("\\.")[1];
//			System.out.println(classes.get(i).getClassName());

			String newPackageName = classes.get(i).getPackage().substring(classes.get(i).getPackage().lastIndexOf(".")+1,classes.get(i).getPackage().length());

			if(!(newPackage.getName().equals(newPackageName))){
				if(newPackage.getName() != ""){
					packages.add(newPackage);
				}
				newPackage = new FlowerComposite();
				newPackage.setName(newPackageName);

				r = rand.nextFloat();
				g = rand.nextFloat();
				b = rand.nextFloat();
				
				newPackage.setPrimaryColor(new Color(r, g, b, 0.5f));
			}
			
			float scaleFactor = (float) classes.get(i).getNumberOfLines() / (float) topStemHeight;
			
			int stemHeight = Math.max(MINSTEMHEIGHT, (int) (classes.get(i).getNumberOfLines() * stemHeightFactor));
			int numberOfPetals = classes.get(i).getMethods().size();
						
			int coreSize = 50;
			int petalRadius = 50;
			
			int numberOfRoots = classes.get(i).getImports().size();
			boolean hasLeaves = true;
			if(scaleFactor < 0.25){
				hasLeaves = false;
			}
			String className = classes.get(i).getSimpleName();
			Flower newFlower = new Flower(0, 0, stemHeight, numberOfPetals, coreSize, petalRadius, numberOfRoots, hasLeaves, className);
			newFlower.setScaleFactor(scaleFactor * 1.2f);
			newPackage.add(newFlower);
		}
		packages.add(newPackage);
	}
	
	public void translateRelationships(int[][] relationships){
		relationList = new ArrayList<FlowerRelation>();
		for(int i = 0; i < relationships.length; i++){
			for(int j = 0; j < relationships[i].length; j++){
				if(relationships[i][j] != 0){
					relationList.add(new FlowerRelation(i, j, relationships[i][j]));
					if(i > j){
						relationList.get(relationList.size()-1).setPrimaryColor(new Color(0.0f, 0.0f, 1.0f));
					}
					else{
						relationList.get(relationList.size()-1).setPrimaryColor(new Color(1.0f, 0.0f, 0.0f));
					}
				}
			}
		}
	}
	
	public TranslatedObjects getTranslatedObjects(){
		TranslatedObjects translated = new TranslatedObjects(packages, relationList, topStemHeight);
		
		return translated;
	}

	@Override
	public TranslatedObjects execute() {
		translateClass(dataObject.getClasses());
		translateRelationships(dataObject.getRelationships());
		
		return getTranslatedObjects();
	}

}

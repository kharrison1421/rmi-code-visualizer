package visualizer.translator;

import java.util.ArrayList;
import visualizer.parser.ClassObject;

public interface ClassTranslator {
	
	public void translateClass(ArrayList<ClassObject> classes);
	public void translateRelationships(int[][] relationships);
	public TranslatedObjects getTranslatedObjects();
}

package visualizer.flowerVisualizer;

import java.awt.Color;
import java.io.Serializable;

//More of a struct
public class FlowerRelation implements Serializable{

	private static final long serialVersionUID = -5544991884127118610L;

	public int fromFlower;
	public int toFlower;
	public int connectionWidth; //Pixels?
	private Color primaryColor = Palette.ORANGE;	

	public FlowerRelation(int fromFlower, int toFlower, int connectionWidth) {
		super();
		this.fromFlower = fromFlower;
		this.toFlower = toFlower;
		this.connectionWidth = connectionWidth;
	}
	
	public Color getPrimaryColor() {
		return primaryColor;
	}

	public void setPrimaryColor(Color primaryColor) {
		this.primaryColor = primaryColor;
	}
}
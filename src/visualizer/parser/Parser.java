package visualizer.parser;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;

import compute.Task;

/**
 * Uses given file path to find java files and parse them, extracting data pertaining to class size, number of methods in 
 * class, classes invoked, methods invoked, package, and imports.
 * 
 * @author Kieran Harrison
 *
 */
public class Parser implements Task<CodeDataObject>, Serializable{
	
	private static final long serialVersionUID = -5620456401452411334L;
	
	private static ClassObject cobj; // An object containing data parsed from each class
	private static MethodObject mobj; // An object containing data parsed from each method in a class
	private static CompilationUnit cu; // Returned by ASTParser after parsing java files. Contains all data to be extracted
	private static ArrayList<ClassObject> classes; // List of objects containing data parsed from each class
	private static int[][] relationships; // List of relationships between classes (method calls)
	private String path; // File path to java src

	/**
	 * Constructor. Sets path to given path.
	 * 
	 * @param path
	 */
	public Parser(String path){
		this.path = path;
	}
	
	/**
	 * Initiates execution of parser
	 */
	@Override
	public CodeDataObject execute() {
		try {
			parseFilesInDir(path);
			relationships = findRelations(classes);
		} catch (IOException e) {
			System.out.println("Error in Parse.execute");
			e.printStackTrace();
		}
		
		return getCodeDataObject();
	}
	
	/**
	 * Initializes classes list and obtains all files using given path as root
	 *  
	 * @param str
	 * @throws IOException
	 */
	public static void parseFilesInDir(String path) throws IOException{
		classes = new ArrayList<ClassObject>();
		File root = new File(path);
		File[] files = root.listFiles ( );
		iterateFiles(files);
	}
	
	/**
	 * Recursively iterates through files looking for .java files
	 * 
	 * @param files
	 * @throws IOException
	 */
	public static void iterateFiles(File[] files) throws IOException{
	    for (File file : files) {
	    	// If file is a directory, go deeper, else, parse file if java file
	        if (file.isDirectory()) {
	            iterateFiles(file.listFiles()); // Go one level deeper in directory
	        } 
	        else if (file.getName().endsWith(".java")){
	        	// Extract simple from full name (ie. extract name from path + name)
	        	String filepath = file.getAbsolutePath();
	        	String[] temp;
	        	if(System.getProperty("os.name").startsWith("Windows"))
	        		temp = filepath.split("\\\\");
	        	else
		        	temp = filepath.split("/");
	        	String[] simpleName = temp[temp.length - 1].split("\\.");
	        	
	        	// Create ClassObject, give it to parser to put desired data in it, then add
	        	// to list of ClassObjects
	        	cobj = new ClassObject();
	        	cobj.setSimpleName(simpleName[0]);
	        	cobj.setClassName(filepath);
	        	parse(readFileToString(filepath));
	        	classes.add(cobj);
	        }
	    }
	}
	
	/**
	 * Sets contents of a file to a single string.
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
 
		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
 
		reader.close();
 
		return  fileData.toString();	
	}
	
	/**
	 * Uses ASTParser to parse file and obtain desired data, adding it to ClassObject.
	 * 
	 * @param str
	 */
	public static void parse(String str){
		// Initialize ASTParser
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setSource(str.toCharArray()); // Set parser to parse a string
		parser.setKind(ASTParser.K_COMPILATION_UNIT); 
		cu = (CompilationUnit) parser.createAST(null);
		cobj.setNumberOfLines(cu.getLineNumber(str.length()-1)); // Add class size to ClassObject
		cu.accept(new ASTVisitor(){ // Go through compilation unit of class to extract desired data
			
			// Add list of classes directly invoked by class being parsed to its ClassObject
			public boolean visit(ClassInstanceCreation node){
				cobj.addInvokedClass(node.getType().toString());
				
				return true;	
			}
			
			// Add name of package class is in to its ClassObject
			public boolean visit(PackageDeclaration node){
				cobj.setPackage(node.getName().toString());
				
				return true;
			}
			
			// Add list of imports used by class to its ClassObject
			public boolean visit(ImportDeclaration node){
				cobj.addImports(node.getName().toString());
		       	String[] temp = node.getName().toString().split("\\\\");
	        	String[] simpleimport = temp[temp.length - 1].split("\\.");
	        	cobj.addSimpleImport(simpleimport[0]);
				
	        	return true;
			}
			
			// Add list of methods in class, including their size
			public boolean visit(MethodDeclaration node){
				// Length of class (lines of code)
				int length = 1 + 
						cu.getLineNumber(node.getLength() + node.getStartPosition()) - 
						cu.getLineNumber(node.getStartPosition());
				
				mobj = new MethodObject();
				mobj.setName(node.getName().toString());
				mobj.setNumberOfLines(length);
				cobj.addMethod(mobj);
				
				return true;
			}
			
			// Add name of class that a called foreign method belongs to to ClassObject
			public boolean visit(MethodInvocation node){
				Expression exp = node.getExpression();
				String str;
				if(exp != null){
					str = exp.toString();
					if(Character.isUpperCase(str.charAt(0)))
						cobj.addInvokedClass(str);
				}
				
				return true;
			}
		});
	}
	
	/**
	 * Returns relations in a metrics form. (ie 0 3 means class 0 uses class 3)
	 * 
	 * @param clist
	 * @return
	 */
	public static int[][] findRelations(ArrayList<ClassObject> clist){
		int[][] relations = new int[clist.size()][clist.size()];
		int k,h;
		
		// Puts zeroes in relation metrics
		for (k = 0 ; k<clist.size(); k++){
			for (h = 0 ; h < clist.size(); h++)
				relations[k][h]= 0;		
		}		
		
		// Fill in metrics
		for (int i=0; i<clist.size(); i++){
			ArrayList<String> invokedClasses = clist.get(i).getInvokedClasses();
			for(String s : invokedClasses){
				for(int j=0; j<clist.size(); j++){
					if(s.equals(clist.get(j).getSimpleName()))
						relations[i][j]++;
				}
			}
		}
		
		return relations;
	}
	
	/**
	 * Puts classes and relationships lists into a single class to be returned by caller
	 * 
	 * @return 
	 */
	public CodeDataObject getCodeDataObject(){
		CodeDataObject object = new CodeDataObject(classes, relationships);
		
		return object;
	}

}

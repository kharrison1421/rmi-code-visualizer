package visualizer.parser;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Object encapsulating list of ClassObjects and relationships
 * 
 * @author Kieran Harrison
 *
 */
public class CodeDataObject implements Serializable{

	private static final long serialVersionUID = 676710130883360603L;

	private ArrayList<ClassObject> classes;
	private int[][] relationships;
	
	public CodeDataObject(ArrayList<ClassObject> classes, int[][] relationships){
		this.classes = classes;
		this.relationships = relationships;
	}
	
	public ArrayList<ClassObject> getClasses(){
		return classes;
	}
	
	public int[][] getRelationships(){
		return relationships;
	}
}

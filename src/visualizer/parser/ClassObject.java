package visualizer.parser;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Object holding data about a class extracted from Parser.
 * 
 * @author Kieran Harrison
 *
 */
public class ClassObject implements Serializable{

	private static final long serialVersionUID = -4167798097136298542L;
	
	private ArrayList<String> imports; // List of imports used by class
	private ArrayList<MethodObject> methods; // List of methods in class
	private ArrayList<String> invokedClasses; // List of classes used by class
	private ArrayList<String> invokedMethod; // List of foreign methods used by class
	private ArrayList<String> simpleimports; // List of imports used by class
	private int numberOfLines; // Number of lines in class
	private String className; // Path + name of class
	private String simpleName; // Name of class
	private String packageName; // Package class belongs to
	
	public ClassObject(){
		imports = new ArrayList<String>();
		methods = new ArrayList<MethodObject>();
		invokedClasses = new ArrayList<String>();
		invokedMethod = new ArrayList<String>();
		simpleimports = new ArrayList<String>();
	}
	public ClassObject(ArrayList<String> imports, ArrayList<MethodObject> methods, int numberOfLines, String className){
		this.imports = imports;
		this.methods = methods;
		this.numberOfLines = numberOfLines;
		this.setClassName(className);
	}

	public void setPackage(String str){
		this.packageName = str;
	}
	public String getPackage(){
		return this.packageName;
	}
	public void addImports (String str){
		this.imports.add(str);
	}
	public ArrayList<String> getImports() {
		return imports;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getClassName() {
		return className;
	}
	public void setSimpleName(String name){
		this.simpleName = name;
	}
	public String getSimpleName(){
		return simpleName;
	}
	public void addMethod(MethodObject mobj){
		this.methods.add(mobj);
	}
	public ArrayList<MethodObject> getMethods() {
		return methods;
	}
	public void addInvokedClass(String className){
		this.invokedClasses.add(className);
	}
	public void addInvokedMethod(String methodName){
		this.invokedMethod.add(methodName);
	}
	public ArrayList<String> getInvokedMethod(){
		return this.invokedMethod;
	}
	public void addSimpleImport(String importName){
		this.invokedMethod.add(importName);
	}
	public ArrayList<String> getSimpleImport(){
		return this.simpleimports;
	}
	public ArrayList<String> getInvokedClasses(){
		return invokedClasses;
	}
	public void setNumberOfLines(int numberOfLines) {
		this.numberOfLines = numberOfLines;
	}
	public int getNumberOfLines() {
		return numberOfLines;
	}
}

package visualizer.parser;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * OObject holding data about a method extracted from Parser.
 * 
 * @author Kieran Harrison
 *
 */
public class MethodObject implements Serializable{

	private static final long serialVersionUID = 1347490717987073600L;
	
	private String name; // Name of method
	private int numberOfLines; // Number of lines in method
	private ArrayList<MethodObject> invocations; // List of foreign methods this method called
	
	public MethodObject(){
		invocations = new ArrayList<MethodObject>();
	}
	public MethodObject(String name, int numberOfLines){
		this.name = name;
		this.numberOfLines = numberOfLines;
	}
	
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public void setNumberOfLines(int numberOfLines){
		this.numberOfLines = numberOfLines;
	}
	public int getNumberOfLines(){
		return numberOfLines;
	}
	public void addInvocation(MethodObject mobj){
		this.invocations.add(mobj);
	}
	public ArrayList<MethodObject> getInvocations(){
		return this.invocations;
	}
}
